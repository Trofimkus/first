﻿namespace WFCalcv2
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.ClearAll = new System.Windows.Forms.Button();
            this.Clear = new System.Windows.Forms.Button();
            this.Backspase = new System.Windows.Forms.Button();
            this.buttonnull = new System.Windows.Forms.Button();
            this.dot = new System.Windows.Forms.Button();
            this.plusminus = new System.Windows.Forms.Button();
            this.plus = new System.Windows.Forms.Button();
            this.multipl = new System.Windows.Forms.Button();
            this.division = new System.Windows.Forms.Button();
            this.percento = new System.Windows.Forms.Button();
            this.equally = new System.Windows.Forms.Button();
            this.minus = new System.Windows.Forms.Button();
            this.factor = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.closeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox1.Location = new System.Drawing.Point(15, 31);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(316, 38);
            this.textBox1.TabIndex = 1;
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(342, 28);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.closeToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(44, 24);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(53, 24);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(14, 245);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(44, 38);
            this.button1.TabIndex = 3;
            this.button1.Text = "1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(64, 245);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(44, 38);
            this.button2.TabIndex = 4;
            this.button2.Text = "2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(114, 245);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(44, 38);
            this.button3.TabIndex = 5;
            this.button3.Text = "3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(14, 201);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(44, 38);
            this.button4.TabIndex = 6;
            this.button4.Text = "4";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(64, 201);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(44, 38);
            this.button5.TabIndex = 7;
            this.button5.Text = "5";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(114, 201);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(44, 38);
            this.button6.TabIndex = 8;
            this.button6.Text = "6";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(15, 157);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(44, 38);
            this.button7.TabIndex = 9;
            this.button7.Text = "7";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(65, 157);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(44, 38);
            this.button8.TabIndex = 10;
            this.button8.Text = "8";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(115, 157);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(44, 38);
            this.button9.TabIndex = 11;
            this.button9.Text = "9";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // ClearAll
            // 
            this.ClearAll.Location = new System.Drawing.Point(15, 113);
            this.ClearAll.Name = "ClearAll";
            this.ClearAll.Size = new System.Drawing.Size(44, 38);
            this.ClearAll.TabIndex = 12;
            this.ClearAll.Text = "CE";
            this.ClearAll.UseVisualStyleBackColor = true;
            this.ClearAll.Click += new System.EventHandler(this.ClearAll_Click);
            // 
            // Clear
            // 
            this.Clear.Location = new System.Drawing.Point(65, 113);
            this.Clear.Name = "Clear";
            this.Clear.Size = new System.Drawing.Size(44, 38);
            this.Clear.TabIndex = 13;
            this.Clear.Text = "C";
            this.Clear.UseVisualStyleBackColor = true;
            this.Clear.Click += new System.EventHandler(this.Clear_Click);
            // 
            // Backspase
            // 
            this.Backspase.Location = new System.Drawing.Point(115, 113);
            this.Backspase.Name = "Backspase";
            this.Backspase.Size = new System.Drawing.Size(44, 38);
            this.Backspase.TabIndex = 14;
            this.Backspase.Text = "BS";
            this.Backspase.UseVisualStyleBackColor = true;
            this.Backspase.Click += new System.EventHandler(this.Backspase_Click);
            // 
            // buttonnull
            // 
            this.buttonnull.Location = new System.Drawing.Point(14, 289);
            this.buttonnull.Name = "buttonnull";
            this.buttonnull.Size = new System.Drawing.Size(44, 38);
            this.buttonnull.TabIndex = 15;
            this.buttonnull.Text = "0";
            this.buttonnull.UseVisualStyleBackColor = true;
            this.buttonnull.Click += new System.EventHandler(this.buttonnull_Click);
            // 
            // dot
            // 
            this.dot.Location = new System.Drawing.Point(65, 289);
            this.dot.Name = "dot";
            this.dot.Size = new System.Drawing.Size(44, 38);
            this.dot.TabIndex = 16;
            this.dot.Text = ",";
            this.dot.UseVisualStyleBackColor = true;
            this.dot.Click += new System.EventHandler(this.dot_Click);
            // 
            // plusminus
            // 
            this.plusminus.Location = new System.Drawing.Point(115, 289);
            this.plusminus.Name = "plusminus";
            this.plusminus.Size = new System.Drawing.Size(44, 38);
            this.plusminus.TabIndex = 17;
            this.plusminus.Text = "+/-";
            this.plusminus.UseVisualStyleBackColor = true;
            this.plusminus.Click += new System.EventHandler(this.plusminus_Click);
            // 
            // plus
            // 
            this.plus.Location = new System.Drawing.Point(187, 245);
            this.plus.Name = "plus";
            this.plus.Size = new System.Drawing.Size(44, 38);
            this.plus.TabIndex = 18;
            this.plus.Text = "+";
            this.plus.UseVisualStyleBackColor = true;
            this.plus.Click += new System.EventHandler(this.plus_Click);
            // 
            // multipl
            // 
            this.multipl.Location = new System.Drawing.Point(187, 201);
            this.multipl.Name = "multipl";
            this.multipl.Size = new System.Drawing.Size(44, 38);
            this.multipl.TabIndex = 19;
            this.multipl.Text = "*";
            this.multipl.UseVisualStyleBackColor = true;
            this.multipl.Click += new System.EventHandler(this.multipl_Click);
            // 
            // division
            // 
            this.division.Location = new System.Drawing.Point(187, 157);
            this.division.Name = "division";
            this.division.Size = new System.Drawing.Size(44, 38);
            this.division.TabIndex = 20;
            this.division.Text = "/";
            this.division.UseVisualStyleBackColor = true;
            this.division.Click += new System.EventHandler(this.division_Click);
            // 
            // percento
            // 
            this.percento.Location = new System.Drawing.Point(187, 113);
            this.percento.Name = "percento";
            this.percento.Size = new System.Drawing.Size(44, 38);
            this.percento.TabIndex = 21;
            this.percento.Text = "%";
            this.percento.UseVisualStyleBackColor = true;
            this.percento.Click += new System.EventHandler(this.percento_Click);
            // 
            // equally
            // 
            this.equally.Location = new System.Drawing.Point(187, 289);
            this.equally.Name = "equally";
            this.equally.Size = new System.Drawing.Size(143, 38);
            this.equally.TabIndex = 22;
            this.equally.Text = "=";
            this.equally.UseVisualStyleBackColor = true;
            this.equally.Click += new System.EventHandler(this.equally_Click);
            // 
            // minus
            // 
            this.minus.Location = new System.Drawing.Point(237, 245);
            this.minus.Name = "minus";
            this.minus.Size = new System.Drawing.Size(44, 38);
            this.minus.TabIndex = 23;
            this.minus.Text = "-";
            this.minus.UseVisualStyleBackColor = true;
            this.minus.Click += new System.EventHandler(this.minus_Click);
            // 
            // factor
            // 
            this.factor.Location = new System.Drawing.Point(287, 245);
            this.factor.Name = "factor";
            this.factor.Size = new System.Drawing.Size(44, 38);
            this.factor.TabIndex = 24;
            this.factor.Text = "!";
            this.factor.UseVisualStyleBackColor = true;
            this.factor.Click += new System.EventHandler(this.factor_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(159, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 25);
            this.label1.TabIndex = 25;
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // closeToolStripMenuItem
            // 
            this.closeToolStripMenuItem.Name = "closeToolStripMenuItem";
            this.closeToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.closeToolStripMenuItem.Text = "Close";
            this.closeToolStripMenuItem.Click += new System.EventHandler(this.closeToolStripMenuItem_Click);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(181, 26);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(342, 335);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.factor);
            this.Controls.Add(this.minus);
            this.Controls.Add(this.equally);
            this.Controls.Add(this.percento);
            this.Controls.Add(this.division);
            this.Controls.Add(this.multipl);
            this.Controls.Add(this.plus);
            this.Controls.Add(this.plusminus);
            this.Controls.Add(this.dot);
            this.Controls.Add(this.buttonnull);
            this.Controls.Add(this.Backspase);
            this.Controls.Add(this.Clear);
            this.Controls.Add(this.ClearAll);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button ClearAll;
        private System.Windows.Forms.Button Clear;
        private System.Windows.Forms.Button Backspase;
        private System.Windows.Forms.Button buttonnull;
        private System.Windows.Forms.Button dot;
        private System.Windows.Forms.Button plusminus;
        private System.Windows.Forms.Button plus;
        private System.Windows.Forms.Button multipl;
        private System.Windows.Forms.Button division;
        private System.Windows.Forms.Button percento;
        private System.Windows.Forms.Button equally;
        private System.Windows.Forms.Button minus;
        private System.Windows.Forms.Button factor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripMenuItem closeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
    }
}

