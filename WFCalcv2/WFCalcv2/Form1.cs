﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WFCalcv2
{
    public partial class Form1 : Form
    {
        float a, b;
        bool znak = true;
        int colc;
        public Form1()
        {
            InitializeComponent();
        }
        private void restartclear()
        {
            switch(colc)
            {
                case 1:
                    a = 0;
                    break;
                case 2:
                    if (textBox1.Text.Equals(""))
                    {
                        a = 2 * float.Parse(label1.Text);
                    }
                    else
                    {
                        a = 2 * float.Parse(textBox1.Text);
                    }
                    break;
                case 3:
                    a = 1;
                    break;
                case 4:
                    if (textBox1.Text.Equals(""))
                    {
                        a = float.Parse(label1.Text) * float.Parse(label1.Text);
                    }
                    else
                    {
                        a = float.Parse(textBox1.Text) * float.Parse(textBox1.Text);
                    }
                        break;
            }
        }
        private void calculation()
        {
           switch (colc)
            {
                case 1:
                    
                    b = a + float.Parse(textBox1.Text); 
                    textBox1.Clear();
                    label1.Text = "";
                    textBox1.Text = b.ToString();
                    restartclear();
                    break;
                case 2:

                    b = a - float.Parse(textBox1.Text);
                    textBox1.Clear();
                    label1.Text = "";
                    textBox1.Text = b.ToString();
                    restartclear();
                    break;
                case 3:
                    
                    b = a * float.Parse(textBox1.Text);
                    textBox1.Clear();
                    label1.Text = "";
                    textBox1.Text = b.ToString();
                    restartclear();
                    break;
                case 4:

                    b = a / float.Parse(textBox1.Text);
                    textBox1.Clear();
                    label1.Text = "";
                    textBox1.Text = b.ToString();
                    restartclear();
                    break;

            }
        }
        private void calculation2()
        {
            switch (colc)
            {
                case 1:

                    b = a + float.Parse(textBox1.Text);
                    textBox1.Clear();
                    label1.Text = "";
                    textBox1.Text = b.ToString();
                    break;
                case 2:

                    b = a - float.Parse(textBox1.Text);
                    textBox1.Clear();
                    label1.Text = "";
                    textBox1.Text = b.ToString();
                    break;
                case 3:

                    b = a * float.Parse(textBox1.Text);
                    textBox1.Clear();
                    label1.Text = "";
                    textBox1.Text = b.ToString();
                    break;
                case 4:

                    b = a / float.Parse(textBox1.Text);
                    textBox1.Clear();
                    label1.Text = "";
                    textBox1.Text = b.ToString();
                    break;

            }
        }
        private void button1_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "1";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "2";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "3";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "4";
        }

        private void button5_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "5";
        }

        private void button6_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "6";
        }

        private void button7_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "7";
        }

        private void button8_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "8";
        }

        private void button9_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "9";
        }

        private void buttonnull_Click(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text + "0";
        }

        private void dot_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.IndexOf(',') == -1)
            {
                textBox1.Text = textBox1.Text + ",";
            }
        }
        private void plus_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Equals(""))
            {
                restartclear();
            }
            else
            {
                calculation2();
                restartclear();
                label1.Text = (float.Parse(textBox1.Text)).ToString();
                a = float.Parse(label1.Text);
                textBox1.Clear();
                colc = 1;
            }
 
        }   

        private void minus_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Equals(""))
            {
                restartclear();
            }
            else
            {
                calculation2();
                restartclear();
                label1.Text = (float.Parse(textBox1.Text)).ToString();
                a = float.Parse(label1.Text);
                textBox1.Clear();
                colc = 2;
            }
       
        }

        private void multipl_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Equals(""))
            {
                restartclear();
            }
            else
            {

                calculation2();
                restartclear();
                label1.Text = (float.Parse(textBox1.Text)).ToString();
                a = float.Parse(label1.Text);
                textBox1.Clear();
                colc = 3;
            }
   
        }
        private void division_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Equals(""))
            {
                restartclear();
            }
            else
            {
                calculation2();
                restartclear();

                label1.Text = (float.Parse(textBox1.Text)).ToString();
                a = float.Parse(label1.Text);
                textBox1.Clear();
                colc = 4;
            }
     
        }
        private void percento_Click(object sender, EventArgs e)
        {

        }

        private void factor_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Equals(""))
            {
            }
            else
            {
                int i;
                long m = 1;
                float n = float.Parse(textBox1.Text);
                for(i=1; i<n+1; i++)
                {
                    m = m * i;
                }
                textBox1.Text = m.ToString();
                restartclear();
            }
        }
        private void equally_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Equals(""))
            {
            }
            else
            {
                calculation();
            }
        }

        private void plusminus_Click(object sender, EventArgs e)
        {
            if (znak == true)
            {

                if (textBox1.Text.IndexOf('-') == -1)
                {
                    textBox1.Text = "-" + textBox1.Text;
                    znak = false;
                }
            }
            else
            {
                textBox1.Text = textBox1.Text.Replace("-", "");
                znak = true;
            }
        }

        private void Clear_Click(object sender, EventArgs e)
        {
            textBox1.Clear();
        }

        private void ClearAll_Click(object sender, EventArgs e)
        {
            restartclear();
            textBox1.Clear();
            label1.Text = "";
            colc = 0;

        }

        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Backspase_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Equals(""))
            {
            }
            else
            {
                int i = textBox1.Text.Length - 1;
                string bs=textBox1.Text;
                textBox1.Clear();
                for (int n = 0; n < i; n++)
                    textBox1.Text = textBox1.Text + bs[n];
            }

        }
    }
}
